package company.tothepoint.graphqlsecuritypocjwt.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;
import company.tothepoint.graphqlsecuritypocjwt.model.Game;
import company.tothepoint.graphqlsecuritypocjwt.model.Player;
import company.tothepoint.graphqlsecuritypocjwt.repository.GameRepository;
import company.tothepoint.graphqlsecuritypocjwt.repository.PlayerRepository;
import company.tothepoint.graphqlsecuritypocjwt.service.GameService;
import company.tothepoint.graphqlsecuritypocjwt.service.PlayerService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class PlayerResolver implements GraphQLResolver<Player> {

    private GameService gameService;
    private PlayerService playerService;

    public List<Game> getGames(Player player) {
        return gameService.getGames(player.getGamesIds());
    }

    public List<Player> getFriends(Player player) {
        return playerService.getFriends(player.getFriendsIds());
    }
}
