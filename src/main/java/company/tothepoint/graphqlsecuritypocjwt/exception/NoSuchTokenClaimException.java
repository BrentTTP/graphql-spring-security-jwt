package company.tothepoint.graphqlsecuritypocjwt.exception;

public class NoSuchTokenClaimException extends RuntimeException {
    public NoSuchTokenClaimException(String s) {
        super(s);
    }
}
