package company.tothepoint.graphqlsecuritypocjwt.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import company.tothepoint.graphqlsecuritypocjwt.model.Game;
import company.tothepoint.graphqlsecuritypocjwt.model.Player;
import company.tothepoint.graphqlsecuritypocjwt.service.GameService;
import company.tothepoint.graphqlsecuritypocjwt.service.PlayerService;
import company.tothepoint.graphqlsecuritypocjwt.service.TokenService;
import graphql.schema.DataFetchingEnvironment;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@AllArgsConstructor
public class QueryResolver implements GraphQLQueryResolver {

    private PlayerService playerService;
    private GameService gameService;
    private TokenService tokenService;

    public List<Player> getPlayers(String name) {
        if (name == null) return playerService.getPlayers();
        else              return playerService.getPlayersWithName(name);
    }

    public Player getPlayer(String id) {
        return playerService.getPlayer(id).orElse(null);
    }

    public List<Game> getGames() {
        return gameService.getGames();
    }

    public Game getGame(String id) {
        return gameService.getGame(id).orElse(null);
    }

    public Player me(DataFetchingEnvironment env) {
        String token = getToken(env);
        tokenService.validateToken(token);

        String role = tokenService.getParamFromToken(token, "role");

        if (role.equals("user")) {
            String email = tokenService.getParamFromToken(token, "email");
            Optional<Player> _player = playerService.getPlayerWithEmail(email);
            return _player.orElse(null);
        }
        return null;
    }

    private String getToken(DataFetchingEnvironment env) {
        HttpHeaders headers = env.getContext();
        List<String> authorization = headers.get("Authorization");
        if (authorization != null && authorization.size() == 1) {
            return authorization.get(0);
        }
        return null;
    }
}
