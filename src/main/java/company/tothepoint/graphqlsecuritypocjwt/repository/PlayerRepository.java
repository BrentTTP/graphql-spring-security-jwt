package company.tothepoint.graphqlsecuritypocjwt.repository;

import company.tothepoint.graphqlsecuritypocjwt.model.Player;
import org.bson.types.ObjectId;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

public interface PlayerRepository extends PagingAndSortingRepository<Player, ObjectId> {

    List<Player> findAll();
    List<Player> findByIdIn(List<String> ids);
    List<Player> findAllByName(String name);

    Optional<Player> findByEmail(String email);
}
