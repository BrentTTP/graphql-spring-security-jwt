package company.tothepoint.graphqlsecuritypocjwt.service;

import company.tothepoint.graphqlsecuritypocjwt.model.Game;
import company.tothepoint.graphqlsecuritypocjwt.repository.GameRepository;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class GameService {

    private GameRepository gameRepository;

    public GameService(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public List<Game> getGames() {
        return (ArrayList<Game>)gameRepository.findAll();
    }

    public Optional<Game> getGame(String id) {
        return gameRepository.findById(new ObjectId(id));
    }

    public List<Game> getGames(List<String> ids) {
        return gameRepository.findByIdIn(ids);
    }

    public void createGame(Game newGame) {
        gameRepository.save(newGame);
    }

    public void removeGame(String id) {
        gameRepository.deleteById(new ObjectId(id));
    }
}
