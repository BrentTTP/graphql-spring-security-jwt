package company.tothepoint.graphqlsecuritypocjwt.resolver;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import company.tothepoint.graphqlsecuritypocjwt.model.Game;
import company.tothepoint.graphqlsecuritypocjwt.model.Player;
import company.tothepoint.graphqlsecuritypocjwt.service.GameService;
import company.tothepoint.graphqlsecuritypocjwt.service.PlayerService;
import company.tothepoint.graphqlsecuritypocjwt.service.TokenService;
import graphql.schema.DataFetchingEnvironment;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@AllArgsConstructor
public class MutationResolver implements GraphQLMutationResolver {

    private PlayerService playerService;
    private GameService gameService;
    private TokenService tokenService;

    public String login(String email, String password, DataFetchingEnvironment env) {
        if (email == null || password == null) {
            return "Input field(s) missing";
        }

        Optional<Player> player = playerService.getPlayerWithEmail(email);

        if (player.isPresent()) {
            if (BCrypt.checkpw(password, player.get().getPassword())) {
                return tokenService.getToken(player.get());
            }
        }

        return "Wrong credentials";
    }

    public String register(String name, Integer age, String email, String password) {
        if (name == null || age == null || email == null || password == null) {
            return "Input field missing";
        }
        if (playerService.getPlayerWithEmail(email).isPresent()) {
            return "User already exists";
        }

        Player player = Player.builder().name(name).age(age).email(email).build();
        player.setPassword(BCrypt.hashpw(password, BCrypt.gensalt(12)));

        return "New player saved: " + email;
    }

    public Game createGame(String title, String genre, Integer ageRestriction, DataFetchingEnvironment env) {
        String token = getToken(env);
        if (tokenService.getParamFromToken(token, "role").equals("user")) {
            Game newGame = Game.builder().title(title).genre(genre).ageRestriction(ageRestriction).build();
            gameService.createGame(newGame);
            return newGame;
        }
        return null;
    }

    public String removeGame(String gameId, DataFetchingEnvironment env) {
        String token = getToken(env);
        if (tokenService.getParamFromToken(token, "role").equals("user")) {
            gameService.removeGame(gameId);
            return gameId;
        }
        return "Not Authorized";
    }

    public String removePlayer(String playerId, DataFetchingEnvironment env) {
        String token = getToken(env);
        if (tokenService.getParamFromToken(token, "role").equals("admin")) {
            playerService.removePlayer(playerId);
            return playerId;
        }
        return "Not Authorized";
    }

    private String getToken(DataFetchingEnvironment env) {
        HttpHeaders headers = env.getContext();
        List<String> authorization = headers.get("Authorization");
        if (authorization != null && authorization.size() == 1) {
            return authorization.get(0);
        }
        return null;
    }
}
