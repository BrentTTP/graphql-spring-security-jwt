package company.tothepoint.graphqlsecuritypocjwt.service;

import company.tothepoint.graphqlsecuritypocjwt.exception.NoSuchTokenClaimException;
import company.tothepoint.graphqlsecuritypocjwt.model.Player;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

@Service
public class TokenService {

    private String key;

    public TokenService(@Value("${secret.key}")String key) {
        this.key = key;
    }

    public String getToken(Player player) {
        Map<String, Object> tokendata = new HashMap<>();
        tokendata.put("email", player.getEmail());
        tokendata.put("name", player.getName());
        tokendata.put("role", "player");

        String token = Jwts.builder()
                .setClaims(tokendata)
                .setSubject(player.getEmail())
                .setExpiration(Date.from(LocalDateTime.now().plusDays(1).atZone(ZoneId.systemDefault()).toInstant()))
                .signWith(SignatureAlgorithm.HS256, key)
                .compact();

        return token;
    }

    public boolean validateToken(String jwsString) {
        Jws<Claims> jwt;

        if (jwsString == null || jwsString.equals("null") || jwsString.equals("") || jwsString.equals("undefined")) {
            throw new NoSuchElementException("No token found");
        }

        jwsString = cutBearerFromToken(jwsString);

        try {
            Jwts.parser()
                    .setSigningKey(key)
                    .parseClaimsJws(jwsString); //Throws ExpiredJWTException, handled in exception.ControllerExceptionHandler
            return true;

        } catch (IllegalArgumentException ex) {
            throw new JwtException("Something went wrong while parsing jwt");
        }
    }

    public String getParamFromToken(String jwsString, String param) {

        jwsString = cutBearerFromToken(jwsString);
        Jws<Claims> jwt = Jwts.parser()
                .setSigningKey(key)
                .parseClaimsJws(jwsString);
        String result = (String) jwt.getBody().get(param);
        if (result == null)
            throw new NoSuchTokenClaimException("Could not find " + param + " in token, token was: " + jwsString);
        return result;
    }

    private String cutBearerFromToken(String jwsString) {
        if (jwsString.contains("Bearer ")) {
            jwsString = jwsString.split(" ")[1];
        }
        return jwsString;
    }

}
