package company.tothepoint.graphqlsecuritypocjwt.service;

import company.tothepoint.graphqlsecuritypocjwt.model.Player;
import company.tothepoint.graphqlsecuritypocjwt.repository.PlayerRepository;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PlayerService {

    private PlayerRepository playerRepository;

    public PlayerService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    public List<Player> getPlayers() {
        return playerRepository.findAll();
    }

    public List<Player> getFriends(List<String> ids) {
        return playerRepository.findByIdIn(ids);
    }

    public List<Player> getPlayersWithName(String name) {
        return playerRepository.findAllByName(name);
    }

    public void addPlayer(Player player) {
        playerRepository.save(player);
    }

    public void removePlayer(String id) {
        playerRepository.deleteById(new ObjectId(id));
    }

    public Optional<Player> getPlayer(String id) {
        return playerRepository.findById(new ObjectId(id));
    }

    public Optional<Player> getPlayerWithEmail(String email) {
        return playerRepository.findByEmail(email);
    }
}
