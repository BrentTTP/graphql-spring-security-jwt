package company.tothepoint.graphqlsecuritypocjwt.repository;

import company.tothepoint.graphqlsecuritypocjwt.model.Game;
import org.bson.types.ObjectId;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface GameRepository extends PagingAndSortingRepository<Game, ObjectId> {

    List<Game> findByIdIn(List<String> ids);
}
