package company.tothepoint.graphqlsecuritypocjwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GraphqlSecurityPocJwtApplication {

	public static void main(String[] args) {
		SpringApplication.run(GraphqlSecurityPocJwtApplication.class, args);
	}
}
