package company.tothepoint.graphqlsecuritypocjwt.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Document(collection = "players")
public class Player {

    private ObjectId id;

    private String name;
    private Integer age;
    private List<String> friendsIds;
    private List<String> gamesIds;

    private String email;
    private String password;
}
